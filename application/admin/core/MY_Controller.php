<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_controller extends CI_Controller {
        
        var $variaveis;
        
        public $data = array();
        public $controller = array();
        public $method = array();
        public $baseUrl = array();
    
        public function __construct() {
            parent::__construct();
            $this->variaveis = null;
            $this->variaveis['urlbase'] = base_url();
            $this->variaveis['controller'] = $this->router->fetch_class();
            $this->variaveis['current'] = $this->router->fetch_method();                        
            
            
            $logado = $this->session->userdata("logado");
            
            if ($this->variaveis['controller'] != "login"| $this->variaveis['current'] != "index"){
                if ($logado != 1) {
                        redirect(base_url('login'));	            
                }
            }else{
                if ($logado == 1) {
                        redirect(base_url('home'));
                }                
            }
            
            
        $this->controller = $this->router->fetch_class();
        $this->baseUrl = base_url();
        $this->method = $this->router->fetch_method();

        $this->data = $this->input->post(NULL, TRUE);

            
        }     
}    

