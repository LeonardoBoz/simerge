<?php

class Usuario_Model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = "tb_usuarios";
    }

    function validalogin($usuario, $senha) {

        $this->db->where('ds_login', $usuario);
        $this->db->where('ds_senha', $senha);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
