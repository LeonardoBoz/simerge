<?php $this->load->view('template/header'); ?>

<div class="row TituloPage text-center">
        <h1><?= $touros['status_page'] ?> Touro</h1>
</div>

<div class="row">
    <div class="container">
        <div class="col-md-10 center-block">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url("cadastroAnimais/salvar/{$touros['cd_touro']}");?>">
                <fieldset>
                    <legend>Geral</legend>
                    <div class="form-group">
                        <label class="col-md-1 control-label">Raça</label>  
                        <div class="col-md-5">
                            <select name="ds_" class="form-control">
                                <option value="Gata"> -- </option>
                                <option value="gato"> Red </option>
                                <option value="gato"> Angus </option>
                                <option value="gato"> Etc </option>
                            </select>
                        </div>
                        
                        <label class="col-md-1 control-label" for="ds_imagem">Imagem</label>  
                        <div class="col-md-5">
                            <input id="ds_imagem" value="" name="ds_imagem" type="file" placeholder="Imagem" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 control-label">Idade</label>  
                        <div class="col-md-5">
                            <input value="<?= $touros['nr_idade'] ?>" name="nr_idade" type="text" placeholder="Idade" class="form-control input-md">
                        </div>
                        
                        <label class="col-md-1 control-label">Peso</label>  
                        <div class="col-md-5">
                            <input value="<?= $touros['nr_peso'] ?>" name="nr_peso" type="text" placeholder="Peso" class="form-control input-md">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="pull-right">
                    <button id="gerais" name="gerais" class="btn btn-success" type="submit">Salvar</button>

                    <a href="<?php echo base_url('cadastroAnimais');?>">
                        <button id="gerais" name="gerais" class="btn btn-danger" type="button">Cancelar</button>
                    </a> 
                    
                </fieldset>
            </form> 
            <?php 
            if($touros['ds_imagem'] != ""){ ?>
            <div class="col-md-3">
                <h4 class="text-center text-muted">Imagem</h4>
                <img class="img-responsive" src="<?= "../../../".$this->variaveis['ds_imagem'] ?>">
            </div>
            <?php } ?>
        </div>
    </div>
</div>


<?php $this->load->view('template/footer'); ?>