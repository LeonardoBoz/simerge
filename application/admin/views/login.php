<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ONG - CBC</title>
    <link rel="icon" type="image/png"  href="imagens/favicon.png">

    <link href="<?php echo base_url('../assets/admin/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('../assets/admin/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('../assets/admin/css/style.css');?>" rel="stylesheet" type="text/css">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
</head>
<body>

    <div class = "container">
        <div class="wrapper">
            <form id="form_login" method="post" enctype="multipart/form-data" action="<?php echo base_url('login');?>">
                
                    <img src="<?php echo base_url('../imagens/logo.png');?>" class="img-responsive">
                    <input type="text" class="form-control" name="usuario" placeholder="Usuário" required="" autofocus="" />
                    <input type="password" class="form-control" name="senha" placeholder="Senha" required=""/>     		  

                    <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Entrar</button>  			

            </form>			
        </div>
    </div>

    <script src="<?php echo base_url('../assets/admin/js/jquery-2.1.0.min.js');?>"></script>
    <script src="<?php echo base_url('../assets/admin/js/bootstrap.js');?>"></script>
    
    
    <script>
     var urlcontroller = "{urlbase}{controller}";
    </script>
    
    <script src="<?php echo base_url('../assets/admin/js/indexstyle.js');?>"></script>
    </body>
</html>
