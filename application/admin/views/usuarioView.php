<?php $this->load->view('template/header'); ?>

<div class="row text-center TituloPage">
    <h1>Usuarios</h1>
</div>

<div class="row">
    <div class="container">
        <a href="<?php echo base_url('cadastroUsuario/incluir')?>" >
            <button type="button" class="btn btn-primary btn-lg">
            <span class="fa fa-plus-circle"></span>Usuário</button>
        </a> 
    </div>        
</div>            

<div class=" row clearfix"></div>     

    <div class="row">
     
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>   
                        <th>codigo</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Data Cadastro</th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                    {usuarios}
                        <tr id="{id_usuario}">
                            <td>{id_usuario}</td>
                            <td>{ds_usuario}</td>
                            <td>{ds_email}</td>
                            <td>{dt_cadastro}</td>

                            <td>
                                <a href="<?php echo base_url('cadastroUsuario/visualizar/{id}');?>">
                                    <button class="btn btn-sucess btn-xs" data-title="vistualizar" >
                                        <span class="fa fa-search"></span>
                                    </button>
                                </a>    
                            </td>                                 

                            <td>
                                <a href="<?php echo base_url('cadastroUsuario/editar/{id}');?>">
                                    <button class="btn btn-primary btn-xs" data-title="Edit"   >
                                        <span class="fa fa-pencil"></span>
                                    </button>
                                </a>  
                            </td>

                            <td>              
                                <a class="remove">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>

                        </tr>
                    {/usuarios}    
                    </tbody>

                </table>

                <div class="clearfix"></div>
            </div>

        </div>
        
    </div>
    


<?php $this->load->view('template/footer'); ?>