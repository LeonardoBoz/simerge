<?php $this->load->view('template/header'); ?>

<div class="row TituloPage text-center">
        <h1>{status_page} Post</h1>
</div>

<div class="row">
    <div class="container">
        <div class="col-md-10 center-block">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url('blog/salvar/{cd_post}');?>">
                <fieldset>
                    <legend>Geral</legend>
                    
                    <div class="form-group">
                        <input id="id" value="{cd_post}"  name="cd_post" type="hidden" style="display: none;">
                        <label class="col-md-1 control-label" for="ds_animal">Titulo</label>  
                        <div class="col-md-11">
                            <input id="ds_animal" value="{ds_titulo}"  name="ds_titulo" type="text" placeholder="Titulo" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="ds_texto">Texto</label>  
                        <div class="col-md-11">
                            <input id="ds_texto" value="{ds_texto}" name="ds_texto" type="text" placeholder="Texto" class="form-control input-md">
                        </div>
                    </div>    
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="ds_imagem">Imagem</label>  
                        <div class="col-md-5">
                            <input id="ds_imagem" value="" name="ds_imagem" type="file" placeholder="Imagem" class="form-control input-md">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="pull-right">
                    <button id="gerais" name="gerais" class="btn btn-success" type="submit">Salvar</button>

                    <a href="<?php echo base_url('blog');?>">
                        <button id="gerais" name="gerais" class="btn btn-danger" type="button">Cancelar</button>
                    </a> 
                    
                </fieldset>
            </form> 
            <?php 
            if($this->variaveis['ds_imagem'] != ""){ ?>
            <div class="col-md-3">
                <h4 class="text-center text-muted">Imagem</h4>
                <img class="img-responsive" src="<?= "../../../".$this->variaveis['ds_imagem'] ?>">
            </div>
            <?php } ?>
        </div>
    </div>
</div>


<?php $this->load->view('template/footer'); ?>