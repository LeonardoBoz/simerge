<?php $this->load->view('template/header'); ?>

<div class="row text-center TituloPage">
    <h1>Touros</h1>
</div>

<div class="row">
    <a href="<?php echo base_url('Touros/add') ?>" >
        <button type="button" class="btn btn-primary btn-lg">
            <span class="fa fa-plus-circle"></span>  Touros 
        </button>
    </a>       
</div>            

<div class=" row clearfix"><br></div>     

<div class="row">     
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>   
                <th class="col-md-2">Raça</th>
                <th class="col-md-2">Idade</th>
                <th class="col-md-2">Peso</th>
                <th class="col-md-4">Imagem</th>
                <th class="col-md-2"></th>
                </thead>
                <tbody>
                    <?php foreach($touros as $k => $array){?>
                    <tr id="<?= $touros[$k]['cd_touro'];?>">
                        <td class="col-md-1"><?= $touros[$k]['cd_raca'];?></td>
                        <td class="col-md-2"><?= $touros[$k]['nr_idade'];?></td>
                        <td class="col-md-2"><?= $touros[$k]['nr_peso'];?></td>
                        <td class="col-md-4"><img class="img-responsive" src="<?= $touros[$k]['ds_imagem'];?>"></td>
                        
                        <td class="col-md-3">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <a href="<?php echo base_url('cadastroAnimais/visualizar/{cd_animal}'); ?>">
                                        <button class="btn btn-sucess btn-xs" data-title="vistualizar" >
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </a>  
                                </div>
                                <div class="col-md-4">
                                    <a href="<?php echo base_url('cadastroAnimais/editar/{cd_animal}'); ?>">
                                        <button class="btn btn-primary btn-xs" data-title="Edit"   >
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </a> 
                                </div>
                                <div class="col-md-4">
                                    <a class="remove">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>  
                    <?php }?>
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>



<?php $this->load->view('template/footer'); ?>