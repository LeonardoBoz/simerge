<?php $this->load->view('template/header'); ?>

<div class="row text-center TituloPage">
    <h1>Post</h1>
</div>

<div class="row">
    <a href="<?php echo base_url('blog/add') ?>" >
        <button type="button" class="btn btn-primary btn-lg">
            <span class="fa fa-plus-circle"></span>  Post 
        </button>
    </a>       
</div>            

<div class=" row clearfix"><br></div>     

<div class="row">     
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>   
                <th class="col-md-2">Titulo</th>
                <th class="col-md-2">Texto</th>
                <th class="col-md-2">Data</th>
                <th class="col-md-2">Imagem</th>
                <th class="col-md-2"></th>
                </thead>
                <tbody>
                    {blog}
                    <tr id="{cd_post}">
                        <td class="col-md-1">{ds_titulo}</td>
                        <td class="col-md-3">{ds_texto}</td>
                        <td class="col-md-2">{dt_cadastro}</td>
                        <td class="col-md-3"><img class="img-responsive" src="{ds_imagem}"></td>
                        <td class="col-md-3">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <a href="<?php echo base_url('blog/visualizar/{cd_post}'); ?>">
                                        <button class="btn btn-sucess btn-xs" data-title="vistualizar" >
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </a>  
                                </div>
                                <div class="col-md-4">
                                    <a href="<?php echo base_url('blog/editar/{cd_post}'); ?>">
                                        <button class="btn btn-primary btn-xs" data-title="Edit"   >
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </a> 
                                </div>
                                <div class="col-md-4">
                                    <a class="remove">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    {/blog}    
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>



<?php $this->load->view('template/footer'); ?>