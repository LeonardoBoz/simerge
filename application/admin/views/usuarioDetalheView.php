<?php $this->load->view('template/header'); ?>

<div class="row TituloPage text-center">
    
        <h1>{status_page} Show</h1>
    
</div>

<div class="row">
    <div class="container">
        <div class="col-md-10 center-block">

            <form class="form-horizontal" method="post" action="<?php echo base_url('usuario/salvar/{id}');?>">
                
                <fieldset>
                    <!-- Form Name -->
                    <legend>Geral</legend>

                    <!-- Text input-->

                    <div class="form-group">

                        <label class="col-md-1 control-label" for="nome">Nome</label>  
                        <div class="col-md-11">
                            <input id="nome" value="{nome}"  name="nome" type="text" placeholder="Nome" class="form-control input-md">
                        </div>
                    </div>  

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="Login">Login</label>  
                        <div class="col-md-11">
                            <input id="login" value="{login}" name="login" type="text" placeholder="Login" class="form-control input-md">
                        </div>
                    </div>     

                    <div class="form-group">
                        <label class="col-md-1 control-label" for="senha">Senha</label>  
                        <div class="col-md-11">
                            <input id="senha" name="senha" value="{senha}" type="text" placeholder="Senha" class="form-control input-md">
                        </div>
                    </div>     
                    
                </fieldset>

                <fieldset class="pull-right">
                    <button id="gerais" name="gerais" class="btn btn-success" type="submit">Salvar</button>

                    <a href="<?php echo base_url('usuario');?>">
                        <button id="gerais" name="gerais" class="btn btn-danger" type="button">Cancelar</button>
                    </a> 
                    
                </fieldset>
                
            </form> 
        </div>
    </div>
</div>


<?php $this->load->view('template/footer'); ?>