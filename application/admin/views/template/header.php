<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>CBC - Admin</title>
    <link rel="icon" type="image/png"  href="imagens/favicon.png">

    <link href="<?php echo base_url('../assets/admin/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('../assets/admin/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('../assets/admin/css/style.css');?>" rel="stylesheet" type="text/css">
    
</head>
<body>

    

<div id="wrapper">
    
    
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- 
            
            @todo colocar o site/contato.
            
            -->
            <a class="navbar-brand" target="_blank" href="http://facebook.com/leo.boz.caitano">
                 <img class="img-responsive" id="icon-sistema" src="<?= base_url('../imagens/logo-sistema.png') ?>">
            </a>
            
        </div>
        
        
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">   
            <li class="dropdown">
                <a href="#" class="dropdown-toggle pull-right" data-toggle="dropdown">Olá Usuário <b class="fa fa-user"> </b><b class="fa fa-angle-down"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#"><i class="fa fa-fw fa-cog"></i> Alterar Senha</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
        
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="<?php echo base_url('Home'); ?>"><i class="fa fa-home"></i> Inicio</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Touros'); ?>"><i class="fa fa-plus-square"></i> Touros</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Cavalos'); ?>"><i class="fa fa-pencil-square-o"></i> Cavalos</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Raças'); ?>"><i class="fa fa-users"></i> Raças</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Blog'); ?>"><i class="fa fa-users"></i> Blog</a>
                </li>
                <li>
                    <a href="<?php echo base_url('Usuarios'); ?>"><i class="fa fa-users"></i> Usuarios</a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">    
    
    