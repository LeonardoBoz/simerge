<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("parser");
        $this->load->model('');
    }    

    public function index(){
        
        $this->parser->parse('home', $this->variaveis);
    } 
}
