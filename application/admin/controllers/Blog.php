<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("parser");
        $this->load->model('blog_model');
    }

    public function index() {
        $this->variaveis['blog'] = $this->blog_model->GetAll('cd_post');

        foreach ($this->variaveis['blog'] as $k => $array) {
            $this->variaveis['blog'][$k]['ds_imagem'] = "../" . $this->variaveis['blog'][$k]['ds_imagem'];
        }

        $this->parser->parse('postView', $this->variaveis);
    }

    public function visualizar($id) {
        $cd = 'cd_post';
        $this->variaveis = $this->blog_model->GetById($id, $cd);

        $this->variaveis['status_page'] = 'Visualizando';
        $this->parser->parse('postDetalheView', $this->variaveis);
    }

    public function add() {
        $this->variaveis['cd_post'] = '0';
        $this->variaveis['status_page'] = 'Cadastrando';
        $this->variaveis['ds_titulo'] = '';
        $this->variaveis['ds_imagem'] = '';
        $this->variaveis['ds_texto'] = '';

        $this->parser->parse('postDetalheView', $this->variaveis);
    }

    public function editar($id) {

        $this->variaveis = $this->blog_model->GetById($id, "cd_post");
        $this->parser->parse('postDetalheView', $this->variaveis);
    }

    public function salvar($id) {

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d');
        $hora = date('H:i');

        if ($id == null) {
            $id = $this->input->post('cd_post');
        }

        $dados['dt_cadastro'] = $date;
        $dados['ds_titulo'] = $this->input->post('ds_titulo');
        $dados['ds_texto'] = $this->input->post('ds_texto');

        $imagem[] = $_FILES['ds_imagem'];

        $configuracao = array(
            'upload_path' => '../imagens/blog/',
            'allowed_types' => array('jpg', 'png', 'jpeg'),
            'file_name' => $imagem[0]['name'],
            'max_size' => '5000'
        );

        if (isset($_FILES['ds_imagem'])) {
            $dados['ds_imagem'] = 'imagens/blog/' . $imagem[0]['name'];
        } else {
            $dados['ds_imagem'] = $this->input->post('ds_imagem');
        }

        $this->load->library('upload');
        $this->upload->initialize($configuracao);

        if ($this->upload->do_upload('ds_imagem')) {
            echo "deu upload!";
            if ($id == 0) {
                $this->blog_model->inserir($dados);
            } else {
                $this->blog_model->atualizar($id, $dados);
            }
            redirect('blog', 'location');
        } else {
            redirect('blog/add', 'location');
        }
    }

    public function excluir() {
        $id = $this->input->get('id');
        $delete = $this->blog_model->excluir($id, 'cd_post');

        return $delete;
    }

}
