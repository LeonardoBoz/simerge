<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Touros extends My_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('touros_model');
    }

    public function index() {
        $dados['touros'] = $this->touros_model->GetAll('cd_touro');
        
        $this->load->view('tourosView', $dados);
    }

    public function visualizar($id) {
        $cd = 'cd_touro';
        $this->variaveis = $this->touros_model->GetById($id, $cd);

        $this->variaveis['status_page'] = 'Visualizando';
        $this->parser->parse('animalDetalheView', $this->variaveis);
    }

    public function add() {
        $dados['touros']['cd_touro'] = '0';
        $dados['touros']['status_page'] = 'Cadastrando';
        $dados['touros']['ds_raca'] = '';
        $dados['touros']['ds_imagem'] = '';
        $dados['touros']['nr_idade'] = '';
        $dados['touros']['nr_peso'] = '';

        $this->load->view('tourosDetalheView', $dados);
    }

    public function editar($id) {
        
        $this->variaveis = $this->animais_model->GetById($id, "cd_animal");
        $this->parser->parse('animalDetalheView', $this->variaveis);
    }

    public function salvar($id) {

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d');
        $hora = date('H:i');

        if ($id == null) {
            $id = $this->input->post('cd_animal');
        }
        
        $dados['dt_cadastro'] = $date;
        $dados['ds_animal'] = $this->input->post('ds_animal');
        $dados['ds_historia_animal'] = $this->input->post('ds_historia_animal');
        $dados['ds_grupo_animal'] = $this->input->post('ds_grupo_animal');

        $imagem[] = $_FILES['ds_imagem'];

        $configuracao = array(
            'upload_path' => '../imagens/animais/',
            'allowed_types' => array('jpg', 'png', 'jpeg'),
            'file_name' => $imagem[0]['name'],
            'max_size' => '5000'
        );

        if(isset($_FILES['ds_imagem'])){
            $dados['ds_imagem'] = 'imagens/animais/' . $imagem[0]['name'];
        }else{
            $dados['ds_imagem'] = $this->input->post('ds_imagem');
            
        }

        $this->load->library('upload');
        $this->upload->initialize($configuracao);

        if ($this->upload->do_upload('ds_imagem')) {
            echo "deu upload!";
            if ($id == 0) {
                $this->animais_model->inserir($dados);
            } else {
                $this->animais_model->atualizar($id, $dados);
            }
            redirect('cadastroAnimais', 'location');
        } else {
            redirect('cadastroAnimais/add', 'location');   
        }
    }

    public function excluir() {
        $id = $this->input->get('id');
        $delete = $this->animais_model->excluir($id, 'cd_animal');
    }

}
