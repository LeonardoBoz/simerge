<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Racas extends My_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('touros_model');
    }

    public function index() {
        $this->variaveis['animais'] = $this->touros_model->GetAll('cd_animal', "ASC");
        
        foreach($this->variaveis['animais'] as $k => $array){
            $this->variaveis['animais'][$k]['ds_imagem'] = "../".$this->variaveis['animais'][$k]['ds_imagem'];
        }
        
        $this->load->view('animaisView', $dados);
    }

    public function visualizar($id) {
        $cd = 'cd_animal';
        $this->variaveis = $this->animais_model->GetById($id, $cd);

        $this->variaveis['status_page'] = 'Visualizando';
        $this->parser->parse('animalDetalheView', $this->variaveis);
    }

    public function add() {
        $this->variaveis['cd_animal'] = '0';
        $this->variaveis['status_page'] = 'Cadastrando';
        $this->variaveis['ds_animal'] = '';
        $this->variaveis['dt_cadastro'] = '';
        $this->variaveis['ds_imagem'] = '';
        $this->variaveis['ds_historia_animal'] = '';

        $this->parser->parse('animalDetalheView', $this->variaveis);
    }

    public function editar($id) {
        
        $this->variaveis = $this->animais_model->GetById($id, "cd_animal");
        $this->parser->parse('animalDetalheView', $this->variaveis);
    }

    public function salvar($id) {

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d');
        $hora = date('H:i');

        if ($id == null) {
            $id = $this->input->post('cd_animal');
        }
        
        $dados['dt_cadastro'] = $date;
        $dados['ds_animal'] = $this->input->post('ds_animal');
        $dados['ds_historia_animal'] = $this->input->post('ds_historia_animal');
        $dados['ds_grupo_animal'] = $this->input->post('ds_grupo_animal');

        $imagem[] = $_FILES['ds_imagem'];

        $configuracao = array(
            'upload_path' => '../imagens/animais/',
            'allowed_types' => array('jpg', 'png', 'jpeg'),
            'file_name' => $imagem[0]['name'],
            'max_size' => '5000'
        );

        if(isset($_FILES['ds_imagem'])){
            $dados['ds_imagem'] = 'imagens/animais/' . $imagem[0]['name'];
        }else{
            $dados['ds_imagem'] = $this->input->post('ds_imagem');
            
        }

        $this->load->library('upload');
        $this->upload->initialize($configuracao);

        if ($this->upload->do_upload('ds_imagem')) {
            echo "deu upload!";
            if ($id == 0) {
                $this->animais_model->inserir($dados);
            } else {
                $this->animais_model->atualizar($id, $dados);
            }
            redirect('cadastroAnimais', 'location');
        } else {
            redirect('cadastroAnimais/add', 'location');   
        }
    }

    public function excluir() {
        $id = $this->input->get('id');
        $delete = $this->animais_model->excluir($id, 'cd_animal');
    }

}
