<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("parser");
        $this->load->model('usuario_model');        
    }    

    public function index(){

        $senha = $this->input->post('senha');
        $usuario = $this->input->post('usuario');
        
        if ($usuario  != '' | $senha != ''){
            if ($this->usuario_model->validalogin($usuario, $senha)){
                $this->session->set_userdata("logado", 1);
                redirect('home');
                
            }else{
                $this->parser->parse('login', $this->variaveis);         
                
            }
        }else{
            $this->parser->parse('login', $this->variaveis);    
        }   
    }
}
