<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("parser");
        $this->load->model('usuario_model');
    }

    public function index() {
        $this->variaveis['usuarios'] = $this->usuario_model->getAll('cd_usuario');
        $this->parser->parse('usuarioView', $this->variaveis);
    }
    
    public function visualizar($id) {
        $this->variaveis = $this->usuario_model->GetById($id);
        
        $this->variaveis['status_page'] = 'Visualizando';
        $this->parser->parse('usuarioDetalheView', $this->variaveis);
    }   

    public function incluir() {
        $this->variaveis['cd_usuario'] = '0';
        $this->variaveis['status_page'] = 'Incluindo';
        $this->variaveis['ds_login'] = '';
        $this->variaveis['ds_email'] = '';
        
        $this->parser->parse('usuarioDetalheView', $this->variaveis);
    }

    public function editar($id){
        $this->variaveis = $this->usuario_model->GetById($id);
        $this->variaveis['status_page'] = 'Editando';
        $this->parser->parse('usuarioDetalheView', $this->variaveis);
    }
    
    public function salvar($id) {
        
        $dados['descricao'] = $this->input->post('descricao');
        $dados['data'] = $this->input->post('data');
        $dados['hora'] = $this->input->post('hora');
        $dados['endereco'] = $this->input->post('endereco');
        $dados['eventofacebook'] = $this->input->post('facebook');
        
        if($id != 0){
            $this->usuario_model->atualizar($id, $dados);
            redirect('cadastroUsuario', 'location');

        }else{
            $this->usuario_model->inserir($dados);
            redirect('cadastroUsuario', 'location');
        }
    }
    
    public function excluir(){
        $id = $this->input->get('id');
        $delete = $this->usuario_model->excluir($id);   
    }
    
    
}

