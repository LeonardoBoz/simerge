{post}
    <div class="row" id="post">         

        <div class="col-xs-12 col-sm-4 col-lg-4 pull-left">
            <a id="imgpost" class="thumbnail"  href="diariodebordo/post/{codigotexto}">
                <img src="{imagemresumo}" alt="">
            </a>
        </div>  


        <div class="col-xs-12 col-sm-8 col-lg-8 pull-right" >

            <div class="row">
                <a class="Titulo" href="diariodebordo/post/{codigotexto}" >
                    <h3>

                        <img src="imagens/manolo.png" class="TituloimgManolo" />    
                        {titulo}
                    </h3>
                </a>
            </div>

            <div class="row">

                <p>
                    {texto}
                </p>


            </div>        

            <div class="row infopost" >
                <a class="pull-left leiamais" href="diariodebordo/post/{codigotexto}">
                     Leia mais <i class="fa fa-ellipsis-h"></i>
                </a>

                <a class="pull-right datapost">
                    <i  class="fa fa-calendar"></i> {data}
                </a> 

            </div>

        </div>
    </div> 

<hr>

{/post}