
    
<!-- The carousel -->
    
<div class="center-block">
        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#transition-timer-carousel" data-slide-to="1"></li>
                <li data-target="#transition-timer-carousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <a href = "https://soundcloud.com/MarcioFulber" class="item active" targe="_blank">
                    <img src="imagens/marciofulber.jpg" />

                    <div class="carousel-caption">
                        <h1 class="carousel-caption-header">CD Autoral</h1>
                        <p class="carousel-caption-text hidden-sm hidden-xs">
                            Clique aqui e ouça o cd completo.
                        </p>
                    </div>
                </a>

                <div class="item">
                    <img src="imagens/capa1.jpg" />
                </div>

                <a class="item">
                    <img src="imagens/capa2.jpg" />
                </a>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
                <span class="fa fa-chevron-left"></span>
            </a>

            <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
                <span class="fa fa-chevron-right"></span>
            </a>
        </div>
    </div>
