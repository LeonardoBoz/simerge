
<div class="row col-lg-12 col-md-12">
        <hr>     
   
        <div>
            <h3 class="reviews">Deixe seu comentário:</h3>

        </div>

        
        <div class="comment-tabs">

            <div class="tab-content">
                

                
                <div class="tab-pane active" id="comments">          
                
                    
                    <ul class="media-list">
                        
                        <li class="media">
                            <a class="btn btn-default btn-circle text-uppercase" type="button" href="#add-comment" role="tab" data-toggle="tab">
                                <span class="fa fa-comment"></span> Add Comentário
                            </a>  
                        </li>    
                        
                        
                        
                        
                        <li class="media">
                            <div class="media-body">
                                <div class="well well-lg">
                                    <h4 class="media-heading text-uppercase reviews">Manolo </h4>

                                    <ul class="media-date text-uppercase reviews list-inline">
                                        <li class="dd">22</li>
                                        <li class="mm">09</li>
                                        <li class="aaaa">2014</li>
                                    </ul>
                                    <p class="media-comment">
                                        Mensagem.
                                    </p>


                                    <a class="btn btn-info btn-circle text-uppercase" data-toggle="collapse" href="#replyOne">
                                        <span class="fa fa-comment"></span> 
                                    1 comentário</a>
                                    <a class="btn btn-warning btn-circle text-uppercase" href="#add-comment" role="tab" data-toggle="tab">
                                        <span class="fa fa-comment"></span> 
                                    Responder</a>
                                </div>              
                            </div>


                            <div class="collapse" id="replyOne">

                                <ul class="media-list">
                                    
                                    
                                    
                                    <li class="media media-replied">
                                        <div class="media-body">
                                            <div class="well well-lg">

                                                <h4 class="media-heading text-uppercase reviews">
                                                    <span class="fa fa-share"></span> Bobs
                                                </h4>

                                                <ul class="media-date text-uppercase reviews list-inline">
                                                    <li class="dd">22</li>
                                                    <li class="mm">09</li>
                                                    <li class="aaaa">2014</li>
                                                </ul>

                                                <p class="media-comment">
                                                     Mensagem.
                                                </p>
                                                
                                            </div>              
                                        </div>
                                    </li>
                                    
                                    
                                    
                                    
                                    
                                </ul> 

                            </div>


                        </li>   

                        
                        
                        
                        
                    </ul> 
                </div>







                <div class="tab-pane" id="add-comment">
                    <form action="#" method="post" class="form-horizontal" id="commentForm" role="form"> 
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Comentário</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="addComment" id="addComment" rows="5"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">                    
                                <button class="btn btn-success btn-circle text-uppercase" type="submit" id="submitComment">
                                    <span class="fa fa-send"></span> Enviar</button>
                                <a class="btn btn-warning btn-circle text-uppercase" href="#comments" role="tab" data-toggle="tab">
                                    <span class="fa fa-back"></span>Voltar</a>
                            </div>
                        </div>  

                    </form>

                </div>






            </div>







        </div>   
    </div>