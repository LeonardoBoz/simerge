
<!-- Agenda -->
    
        
<div class="widget-sidebar row">

    <h2 class="title-widget-sidebar">
        <i class="fa fa-calendar" data-original-title="" title=""></i> Agenda</h2>


    {agenda}
    
        <div class="last-post">
            <button class="accordion">{titulo}</button>
            <div class="panel">

                <li class="recent-post">

                    <h5>
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        {endereco}
                    </h5>
                </li>

                <li class="recent-post">

                    <h5>
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        {horario}
                    </h5>
                </li>


                <li class="recent-post">
                    <a href=" {eventoface}; ?>">
                        <h5>
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            Saiba Mais
                        </h5>                            
                    </a>
                </li>
            </div>
        </div>  
    
    {/agenda}
</div>

        
  
 