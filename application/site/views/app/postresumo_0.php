<div class="row" id="post">         
    
    <div class="col-xs-12 col-sm-4 col-lg-4 pull-left">
        
        <a id="imgpost" class="thumbnail"  href="<?php echo base_url('diariodebordo/post/lancamentochapeuvirtual'); ?>">
            <img src="<?php echo base_url('imagens/apoiase.jpg'); ?>" alt="">
        </a>
    </div>  
    

    <div class="col-xs-12 col-sm-8 col-lg-8 pull-right" >
        
        <div class="row">
            <a class="Titulo" href="<?php echo base_url('diariodebordo/post/lancamentochapeuvirtual'); ?>" >
                <h3>
                    <img src="<?php echo base_url('imagens/manolo.png'); ?>" class="TituloimgManolo" />    
                    Lançamento do Chapéu Virtual

                </h3>
            </a>
        </div>

        <div class="row">

            <p>
                Está no ar o nosso Chapéu Virtual!!
                Aos poucos estamos caminhando para realizar nossos sonhos. 
                Depois das diversas viagens realizadas de carona pelo Brasil, para dar continuidade
                ao sonho nômade de levar educação, cultura e arte para todos os lugares que conserguirmos...
            </p>


        </div>        

        <div class="row infopost">
            <a class="pull-left leiamais" href="<?php echo base_url('diariodebordo/post/lancamentochapeuvirtual'); ?>">
                 Leia mais <i class="fa fa-ellipsis-h"></i>
            </a>

            <a class="pull-right datapost">
                <i  class="fa fa-calendar"></i> 16 Outubro, 2017
            </a> 

        </div>
        
    </div>
</div>   