$(function () {
    
    $('[data-toggle="tooltip"]').tooltip();
    $(".side-nav .collapse").on("hide.bs.collapse", function () {
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function () {
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");
    });
    
    jQuery(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");
        
        if(confirm('Tem certeza que deseja excluir esse registo?'))
        {
            $.ajax({
               url: urlcontroller+'/excluir',
               type: 'GET',
               data: {id: id},
               error: function() {
                  alert('Algo deu Errado!');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Removido com sucesso!");  
               }
            });
        }
    });    
    
    
});
