
jQuery('#box').load(function (e) {
    e.preventDefault();

    var animacao = "animated bounce";
    var fimAnimacao = "webkitAnimatedEnd mozAnimatedEnd MSAnimatedEnd oAnimatedEnd animatedEnd";

    jQuery('#box').addClass(animacao).one(fimAnimacao, function () {
        jQuery(event.target).removeClass();
    });

});

$(function () {
    $(window).scroll(function ()
    {
        if ($(this).scrollTop() > 200)
        {
            $('#navbar').addClass('navbar-fixed-top', 500);

        } else {
            $('#navbar').removeClass('navbar-fixed-top', 500);

        }
    });
});

//tab menu
$(document).ready(function (e) {
    $('#menu_icon').click(function () {
        if ($("#content_details").hasClass('drop_menu')) {
            $("#content_details").addClass('drop_menu1').removeClass('drop_menu');
        } else {
            $("#content_details").addClass('drop_menu').removeClass('drop_menu1');
        }

        if ($("#menu_icon").hasClass('fa-bars')) {
            $("#menu_icon").addClass('fa-close').removeClass('fa-bars');
        } else {
            $("#menu_icon").addClass('fa-bars').removeClass('fa-close');
        }

    });

    // agenda         
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }

    var loca = location.href.split("/").slice(-1);
    // menu active
    $('.nav li a').each(function () {
        var atual = $(this).attr('href').split("/").slice(-1);

        if (atual[0] == loca[0]) {
            $(this).addClass('menuactive');
        } else if (loca[0] == "") {
            loca[0] = "index.php";
            $(this).addClass('menuactive');
        }
    });

});



//enviar email
$(document).ready(function () { //Quando documento estiver pronto
    $('#enviaremail').click(function () { /* Quando clicar em #btn */
        /* Coletando dados */
        var nome = $('#nome').val();
        var email = $('#email').val();
        var msg = $('#mensagem').val();

        /* Validando*/
        if (nome.length <= 3) {
            alert('Informe seu nome');
            return false;
        }

        if (email.length <= 5) {
            alert('Informe seu email');
            return false;
        }

        if (msg.length <= 5) {
            alert('Escreva uma mensagem');
            return false;
        }

        /* construindo url */
        var urlData = "&nome=" + nome +
                "&email=" + email +
                "&msg=" + msg;

        /* Ajax */
        $.ajax({
            type: "POST",
            url: "enviaremail.php", /* endereço do script PHP */
            async: true,
            data: urlData, /* informa Url */
            success: function (data) { /* sucesso */
                $('#retornoHTML').html(data);
            },
            beforeSend: function () { /* antes de enviar */
                $('.loading').fadeIn('fast');
            },
            complete: function () { /* completo */
                $('.loading').fadeOut('fast'); //wow!
            }
        });
    });
});

     