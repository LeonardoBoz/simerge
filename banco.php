CREATE TABLE tb_blog (
	cd_post INT(6) NOT NULL AUTO_INCREMENT,
	ds_titulo VARCHAR(200) NOT NULL,
	ds_imagem VARCHAR(200) NOT NULL,
	ds_texto VARCHAR(200) NOT NULL,
        dt_cadastro DATE NOT NULL,
        
	PRIMARY KEY (cd_post)
);

CREATE TABLE tb_touros (
	cd_touro INT(6) NOT NULL AUTO_INCREMENT,
	cd_raca VARCHAR(200) NOT NULL,
	ds_imagem VARCHAR(200) NOT NULL,
	nr_idade VARCHAR(200) NOT NULL,
	nr_peso VARCHAR(200) NOT NULL,
        dt_cadastro DATE NOT NULL,
        
	PRIMARY KEY (cd_touro)
);

CREATE TABLE tb_cavalos (
	cd_cavalo INT(6) NOT NULL AUTO_INCREMENT,
	cd_raca VARCHAR(200) NOT NULL,
	ds_imagem VARCHAR(200) NOT NULL,
	nr_idade VARCHAR(200) NOT NULL,
	nr_peso VARCHAR(200) NOT NULL,
        dt_cadastro DATE NOT NULL,
        
	PRIMARY KEY (cd_cavalo)
);

CREATE TABLE tb_racas (
	cd_raca INT(6) NOT NULL AUTO_INCREMENT,
	ds_raca VARCHAR(200) NOT NULL,
        ds_tipo_animal VARCHAR(200) NOT NULL,
        
	PRIMARY KEY (cd_raca)
);

CREATE TABLE tb_usuarios (
	cd_usuario INT(6) NOT NULL AUTO_INCREMENT,
	ds_login VARCHAR(200) NOT NULL,
	ds_senha VARCHAR(200) NOT NULL,
	ds_nome VARCHAR(200) NOT NULL,
        
	PRIMARY KEY (cd_usuario)
);

INSERT INTO tb_usuarios VALUES ('0','admin','admin','administrador');